import 'material-design-icons-iconfont/dist/material-design-icons.css' // Ensure you are using css-loader
import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import _var from './config/var'
import VueSocketio from 'vue-socket.io-extended';
import io from 'socket.io-client';

import { store } from './store'

Vue.use(Vuetify)
Vue.use(VueSocketio, io(_var.host + ":" + _var.port), {store})
Vue.use(require('vue-moment'));

new Vue({
  el: '#app',
  store,
  render: h => h(App)
})

