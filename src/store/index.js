import Vue from 'vue'
import Vuex from 'vuex'
// import chatSnackbar from '../components/Snackbar'

import _var from '../config/var'
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        chats: [],
        username: "",
        room_id: 0,
        room_name: "Public Chat",
        members:{},
        is_logged_in: false,
        jwt:"",
        socket_id:"",
    },
    getters: {
        CHATS: state => {
            return state.chats
        },
        USER: username => {
            return state.username
        },
        getMembers: state => {
            return state.members
        },
        IS_LOGGED_IN: state => {
            return state.is_logged_in
        },
        ROOM_ID: state => {
            return state.room_id
        },
        ROOM_NAME: state => {
            return state.room_name
        }
    },
    mutations: {
        /* SOCKET_CONNECT: (state, status) => {
            state.connect = true;
            
        }, */
        SET_CHAT: (state, payload) => {
            state.chats = payload;
        },
        SET_USER: (state, payload) => {
            state.username = payload.username;
        },
        SET_LOGGED_IN: (state, payload) => {
            state.is_logged_in = payload;
        },
        SET_SOCKET_ID: (state, payload)=>{
            state.socket_id = payload;
        },
        SET_MEMBERS: (state, payload)=>{
            state.members = payload;
        },
        SET_ROOM(state, payload) {
            state.room_name = payload.room_name
            state.room_id = payload.room_id
        },
        PUSH_CHAT(state, payload){
            state.chats.push(payload);
        }
    },
    actions: {
        ADD_CHAT: (context, payload) => {
            context.commit("ADD_CHAT", payload);
        },
        socket_iJoined: (context, payload) => {
            console.log("socket_iJoined");
            context.commit('SET_MEMBERS', payload.members);
            context.commit("SET_USER", payload);
            context.commit("SET_LOGGED_IN", true);
        },
        socket_someoneJoined:(context, payload) => {
            context.commit('SET_MEMBERS', payload.members)
        },
        socket_refreshMemberList:(context, payload) => {
            context.commit('SET_MEMBERS', payload)
        },
        socket_viewChatList: (context, payload)=>{
            context.commit("SET_CHAT", payload);
        },
        setRoom: (context, payload)=>{
            context.commit("SET_ROOM", payload);
        },
        socket_newPublicMessage:(context, payload)=>{
            if (context.state.room_id==0)
                context.commit("PUSH_CHAT", payload)
        },
        socket_newPrivateMessage: (context, payload)=>{
            console.log('vuex : i got a new private chat')
            console.log(payload)
            console.log(context.state.room_id)
            
            if (payload.username == context.state.room_id){
                context.commit("PUSH_CHAT", payload)
            }
        },
        setSocketId: (context, payload)=>{
            context.commit('SET_SOCKET_ID', payload);
        }
    },
    plugins: [createPersistedState({ key: 'chat', paths: ['username', 'is_logged_in', 'jwt', 'socket_id', 'username', 'room_id', 'room_name']})]

})